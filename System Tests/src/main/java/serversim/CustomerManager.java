package serversim;

import customerbase.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerManager {
    private List<Customer> database;

    public CustomerManager(){
        database = new ArrayList<>();
    }

    public List<Customer> getDatabase() {
        return database;
    }

    public void setDatabase(List<Customer> database) {
        this.database = database;
    }

    public boolean registerCustomer(Customer customer){
        try{
            database.add(customer);
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean removeCustomer(Customer customer){
        try{
            database.remove(customer);
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}

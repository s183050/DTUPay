import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import customerbase.Customer;
import customersim.CustomerSimulator;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class stepDefinitions {
    private BankService bank = new BankServiceService().getBankServicePort();
    private List<String> accounts = new ArrayList<>();

    @After
    public void cleanup() throws Throwable{
        for (String a : accounts){
            bank.retireAccount(a);

        }
    }

    @Given("^a registered customer with a bank account$")
    public void a_registered_customer_with_a_bank_account() throws Throwable {
        Customer customer = new Customer("Hannah","Christoffersen","180364-1796");
        User user = new User();
        user.setFirstName(customer.getFirstName());
        user.setLastName(customer.getLastName());
        user.setCprNumber(customer.getCprNumber());
        String account = bank.createAccountWithBalance(user,new BigDecimal(9999));
        accounts.add(account);
        customer.setAccount(account);
        CustomerSimulator customerSimulator = new CustomerSimulator();
        // customerSimulator.registerCustomer(customer);

    }

    @Given("^a registered merchant with a bank account$")
    public void a_registered_merchant_with_a_bank_account() throws Throwable {
//        Merchant merchant = new Merchant("Søren","Lauridsen","021145-1351");
//        User user =new User();
//        user.setFirstName(merchant.getFirstName());
//        user.setLastName(merchant.getLastName());
//        user.setCprNumber(merchant.getCprNumber());
//        String account = bank.createAccountWithBalance(user,new BigDecimal(2000));
        //accounts.add(account);
        //merchant.setAccount(account);
        //MerchantSimulator merchantSimulator = new MerchantSimulator();
        //MerchantSimulator.registerMerchant(merchant);
    }

    @Given("^the customer has one unused token$")
    public void the_customer_has_one_unused_token() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^the merchant scans the token$")
    public void the_merchant_scans_the_token() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^requests payment for (\\d+) kroner using the token$")
    public void requests_payment_for_kroner_using_the_token(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // Hubert here
        throw new PendingException();
    }

    @Then("^the payment succeeds$")
    public void the_payment_succeeds() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the money is transferred from the customer bank account to the merchant bank account$")
    public void the_money_is_transferred_from_the_customer_bank_account_to_the_merchant_bank_account() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

}

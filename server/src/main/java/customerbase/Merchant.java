package customerbase;

public class Merchant {

    private String firstName;
    private String lastName;
    private String CPR;
    private String account;

    public Merchant(String firstName, String lastName, String CPR){
        this.firstName=firstName;
        this.lastName=lastName;
        this.CPR=CPR;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCprNumber() {
        return CPR;
    }

    public String getAccount(){ return account;  }

    public void setAccount(String account) {
        this.account = account;

    }
}

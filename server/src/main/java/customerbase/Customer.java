package customerbase;

public class Customer {
    private String firstname;
    private String lastname;
    private String cprNumber;
    private String account;



    public Customer(String firstname, String lastname, String cprNumber){
        this.firstname = firstname;
        this.lastname = lastname;
        this.cprNumber = cprNumber;
    }

    public Customer(){

    }

    public void setCprNumber(String cprNumber) {
        this.cprNumber = cprNumber;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstName() { return firstname; }

    public String getLastName() {
        return  lastname;
    }

    public String getCprNumber() {
        return cprNumber;
    }

    public String getAccount(){ return account;  }

    public void setAccount(String account) {
        this.account = account;
    }
}

package serversim;

import customerbase.Customer;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/customer")
public class ServerSimulator {
    private static CustomerManager customerManager = new CustomerManager();


    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerCustomer(Customer cr) {
        try {
            String response = customerManager.registerCustomer(cr) + "size: " + customerManager.getDatabase().size();
            //System.out.println("Server side: " + response);
            return Response.status(201).entity(response).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }

    @Path("/test")
    @POST
  //  @Produces("text/plain")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerCustomer2(Customer cr){
        try {
            String response = customerManager.registerCustomer(cr) + "size: " + customerManager.getDatabase().size();
            //System.out.println("Server side: " + response);
            return Response.status(201).entity(response).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }

    @Path("/test")
    @GET
    @Produces("text/plain")
    public Response doGet(){
        return  Response.ok("Hello there customer!").build();
    }

    @Path("/use/{tokenid}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String useToken(@PathParam("tokenid") String tokenid) {
        try {
            return "The message is: " + tokenid;
        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }
}